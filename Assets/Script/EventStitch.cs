using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using System.Linq;
using System;

public class EventStitch : MonoBehaviour
{
    List<List<Vector3>> garmentparts = new List<List<Vector3>>();
    List<ObiSkinnedCloth> garments = new List<ObiSkinnedCloth>();
    Dictionary<String, List<Vector3>> clothParticlePairs = new Dictionary<String, List<Vector3>>();


    private void Start()
    {
        CreateObiBP.obiAdded += Smethod;
    }


    public void Smethod()
    {

        GameObject Solver = this.gameObject;        
        Debug.Log("Loading stitching script...");
        
        // find garment submeshes 
        int garmentID = 0; // num of submeshes 
        foreach (Transform childobject in Solver.transform)
        {
            //if ( childobject.name.Contains("3DTrim_") ) continue;  // skip the high detailed garment accessaries
            //if ( childobject.name.Contains("Genesis2") ) continue; // skip skeleton node

            List<Vector3> clothParticles = new List<Vector3>();
            ObiSkinnedCloth cloth = childobject.GetComponent<ObiSkinnedCloth>();
            if (cloth == null) continue;

            for (int i = 0; i < cloth.solverIndices.Length; i++)
            {
                clothParticles.Add(cloth.GetParticlePosition(cloth.solverIndices[i]));
            }

            garments.Add(cloth); // obi skinned cloth
            garmentparts.Add(clothParticles); // cloth particles 
            Debug.Log("Garment submesh: " + childobject.name + ", Number of Vertices: " + garmentparts[garmentID].Count);
            garmentID++;

            clothParticlePairs.Add(childobject.name, clothParticles);
        }

        // Create all possible Cloth Pairs
        // note by DS: 
        //      * it seems to be O(n^2) time complexity => can it be any simpler?
        //      * order of a pair is not important (i.e. (a,b) == (b,a)) but this is not rulled out here
        List<Tuple<ObiSkinnedCloth, ObiSkinnedCloth>> garmentpairs = new List<Tuple<ObiSkinnedCloth, ObiSkinnedCloth>>();

        foreach (ObiSkinnedCloth cloth1 in garments)
        {
            foreach (ObiSkinnedCloth cloth2 in garments)
            {
                if (cloth1 != cloth2)
                {
                    garmentpairs.Add(new Tuple<ObiSkinnedCloth, ObiSkinnedCloth>(cloth1, cloth2));
                }
            }
        }

        // eliminate dublicate pairs
        List<Tuple<ObiSkinnedCloth, ObiSkinnedCloth>> uniqueGarmentPairs = new List<Tuple<ObiSkinnedCloth, ObiSkinnedCloth>>();
        foreach (Tuple<ObiSkinnedCloth, ObiSkinnedCloth> t in garmentpairs)
        {
            if (!uniqueGarmentPairs.Contains(t) && !uniqueGarmentPairs.Contains(new Tuple<ObiSkinnedCloth, ObiSkinnedCloth>(t.Item2, t.Item1)))
            {
                uniqueGarmentPairs.Add(t);
            }
        }

        // Create a list with stitchers for each garment pair
        // DS: can we combine the followting 2 for loops??
        List<ObiStitcher> StitchersList = new List<ObiStitcher>();

        // create stitcher list
        for (int i = 0; i < uniqueGarmentPairs.Count; i++)
        {
            ObiStitcher Stitcher = gameObject.AddComponent<ObiStitcher>();
            Stitcher.Actor1 = uniqueGarmentPairs[i].Item1;
            Stitcher.Actor2 = uniqueGarmentPairs[i].Item2;
            StitchersList.Add(Stitcher);
        }

        // actual stitching 
        int count = 0;
        foreach (var t in uniqueGarmentPairs)
        {
            var cloth1name = t.Item1.name;
            var cloth2name = t.Item2.name;
            List<Vector3> clothParticles1 = clothParticlePairs[cloth1name];
            List<Vector3> clothParticles2 = clothParticlePairs[cloth2name];
            ObiStitcher stitchPair = StitchersList[count];
            StitchPairs(clothParticles1, clothParticles2, stitchPair);
            count++;
        }
        
    }


    public void StitchPairs(List<Vector3> clothParticles1, List<Vector3> clothParticles2, ObiStitcher stitcher)
    {
        for (int i = 0; i < clothParticles1.Count; i++)
        {
            for (int j = 0; j < clothParticles2.Count; j++)
            {
                
                
                // Vector3 a = clothParticles1[i];
                // Vector3 b = clothParticles2[j];
                // a.z = 0;
                // b.z = 0;
                // float dist = Vector3.Distance(a, b);
                // if (dist < 0.001){
                //     Debug.Log(dist);
                //     stitcher.AddStitch(i, j);
                // }

                if (Mathf.Approximately(clothParticles1[i].y, clothParticles2[j].y))                
                {                    
                    //keep the particles that have only the same x positions
                    if (Mathf.Approximately(clothParticles1[i].x, clothParticles2[j].x))
                    {
                        stitcher.AddStitch(i, j);
                    }
                }
            }
        }
        stitcher.PushDataToSolver();
    }
    
}
