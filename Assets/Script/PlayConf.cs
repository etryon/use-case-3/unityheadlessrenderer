using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayConf{
    public string targetGarment;
    public int playMode;            // 0: data rec mode 1: play mode
    public int renderingMode;       // 0: all 1: grament only 2: body only 

    // adv. conf 
    // public bool obiParamExport;     // true: produce param.txt false: read from param.txt 
                                       // meaningful when needNewObiAsset == false
    public bool needNewObiAssets;      // recreate obi assets
    public bool useObi;

    // cloth sim parameters 
    public float SRs;
    public float BRs;
    public float BS;
}
