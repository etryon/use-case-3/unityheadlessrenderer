using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Playables;


public class ModeSelector : MonoBehaviour
{
    PlayConf myConfig;

    public GameObject abcAnim;
    public GameObject obiAnim;
    public GameObject dataRec;
    public Material matCorr;  // garment colour for correslation cost 


    // Start is called before the first frame update
    void Start()
    {
        // read file 
        bool isConfigReady = ReadConfigFile();

        if (!isConfigReady){
            Debug.Log("config.json is not ready. cannot run");
        }

        if ( !ActivateTarget() ){
            Debug.Log("cannot find the target model. check if it is ready.");
        }
                                    
        ActivateRenderingObjects();
        SetObiCloth();
        SetDataRecoder();

        if (this.myConfig.playMode == 0){     // data recording mode
            SetForDataRecording();
        }  
        else if (this.myConfig.playMode == 1){ // simulation display mode
            SetForSimulationTesting();
        }    
    }


    bool ActivateTarget(){
        string targetName = this.myConfig.targetGarment;
        // Debug.Log(targetName);
        GameObject targetObj = GameObject.Find(targetName);
        bool res = false;

        object[] obj = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects(); 
        // GameObject.FindSceneObjectsOfType(typeof (GameObject));
        foreach (object o in obj)
        {
            GameObject g = (GameObject) o;
            if (g.name == "DirectionalLight" ||
                g.name == "MainCamera" ||
                g.name == "InitScene" ||
                g.name == "DataRec"){
                g.SetActive(true);
            }
            else if (g.name == targetName){
                abcAnim = targetObj.transform.Find("abc_anim").gameObject;
                obiAnim = targetObj.transform.Find("obi_anim").gameObject;             
                g.SetActive(true);
                res = true;
            }                
            else
                g.SetActive(false);
            // Debug.Log(g.name);
        }


        // if (targetObj){            
        //     abcAnim = targetObj.transform.Find("abc_anim").gameObject;
        //     obiAnim = targetObj.transform.Find("obi_anim").gameObject;
        //     res = true;
        // }
        // else{
        //     Debug.Log("cannot find the target obj!");
        // }

        return res;
    }


    void SetObiCloth()
    {
        GameObject obiGarment = this.obiAnim.transform.Find("Garment").gameObject;
        CreateObiBP obiBP = obiGarment.GetComponent<CreateObiBP>();

        obiBP.garmentDir = this.myConfig.targetGarment;
        obiBP.enabled = this.myConfig.useObi;
        obiBP.needToCreateAssets = this.myConfig.needNewObiAssets;                            
        obiBP.priorStatScale = this.myConfig.SRs;
        obiBP.backstopRadiusScale = this.myConfig.BRs;
        obiBP.backstop = this.myConfig.BS;

        // obiGarment.SetActive(true);
        obiBP.enabled = true;
        // disable obi if needed        
        Debug.Log($"parameter scale from {obiBP.garmentDir}: {obiBP.priorStatScale} {obiBP.backstopRadiusScale} {obiBP.backstop}");
    }


    void SetDataRecoder(){
        GameObject dataRec = GameObject.Find("DataRec");
        CaptureData capData = dataRec.GetComponent<CaptureData>();

        capData.abcGameObj = this.abcAnim;
        capData.obiGameObj = this.obiAnim;
        capData.enabled = true;
    }


    bool ReadConfigFile()
    {
        bool res = true;
         
        string filePath = Application.dataPath + $"/Temp/config.json";
        if(File.Exists(filePath))
        {
            string jsonString = File.ReadAllText(filePath);
            if (jsonString.Length > 0) 
            {
                this.myConfig = JsonUtility.FromJson<PlayConf>(jsonString);
                Debug.Log(this.myConfig.needNewObiAssets);
            }
            else
                res = false;
        }
        else
            res = false;

        return res;
    }


    void ActivateRenderingObjects()
    {
        GameObject obiAvatar = this.obiAnim.transform.Find("Avatar").gameObject;
        GameObject obiGarment = this.obiAnim.transform.Find("Garment").gameObject;

        GameObject abcAvatar = this.abcAnim.transform.Find("Avatar").gameObject;
        GameObject abcGarment = this.abcAnim.transform.Find("Garment").gameObject;

        CaptureData capture = this.dataRec.GetComponent<CaptureData>();
        
        // render garment only
        if (this.myConfig.renderingMode == 1) 
        {
          obiAvatar.SetActive(false); 
          obiGarment.SetActive(true);
          abcAvatar.SetActive(false); 
          abcGarment.SetActive(true);
          capture.suffix = "_g";
        }
        // render body only
        else if (this.myConfig.renderingMode == 2) 
        {
          obiAvatar.SetActive(true); 
          obiGarment.SetActive(false);
          abcAvatar.SetActive(true); 
          abcGarment.SetActive(false);
          capture.suffix = "_b";
        }
        // render body and garment all
        else 
        {
          obiAvatar.SetActive(true); 
          obiGarment.SetActive(true);
          abcAvatar.SetActive(true); 
          abcGarment.SetActive(true);
          capture.suffix = "_gb";
        }
    }
    

    void SetForDataRecording()
    {        

        // disable animation set manual                  
        GameObject avatar = this.obiAnim.transform.Find("Avatar").gameObject;
        GameObject garment = this.obiAnim.transform.Find("Garment").gameObject;
        GameObject abc_garment = this.abcAnim.transform.Find("Garment").gameObject;
        Animator avatarAnim = avatar.GetComponent<Animator>();       
        Animator garmentAnim = garment.GetComponent<Animator>();    
        avatarAnim.enabled = false;
        garmentAnim.enabled = false;   

        PlayableDirector playableDirector = this.abcAnim.GetComponent<PlayableDirector>();
        playableDirector.timeUpdateMode = DirectorUpdateMode.Manual;
        playableDirector.playOnAwake = false;

        // enable data rec 
        this.dataRec.SetActive(true);
        this.dataRec.GetComponent<CaptureData>().enabled = true;

        // chnage colour for bw         
        Renderer[] renderers = garment.GetComponentsInChildren<Renderer>();
        for(int i = 0; i < renderers.Length; i++) {
            //The line below "resets" the materials
            renderers[i].material = matCorr;
        }
        renderers = abc_garment.GetComponentsInChildren<Renderer>();
        for(int i = 0; i < renderers.Length; i++) {
            //The line below "resets" the materials
            renderers[i].material = matCorr;
        }

    }


    void SetForSimulationTesting()
    {
        
        // enable animation 
        GameObject avatar = this.obiAnim.transform.Find("Avatar").gameObject;
        GameObject garment = this.obiAnim.transform.Find("Garment").gameObject;
        GameObject abc_garment = this.abcAnim.transform.Find("Garment").gameObject;
        Animator avatarAnim = avatar.GetComponent<Animator>();       
        Animator garmentAnim = garment.GetComponent<Animator>();    
        avatarAnim.enabled = true;
        garmentAnim.enabled = true; 

        PlayableDirector playableDirector = this.abcAnim.GetComponent<PlayableDirector>();
        playableDirector.timeUpdateMode = DirectorUpdateMode.GameTime;
        playableDirector.playOnAwake = true;
        playableDirector.Play();


        // chnage colour for bw         
        // Renderer[] renderers = garment.GetComponentsInChildren<Renderer>();
        // for(int i = 0; i < renderers.Length; i++) {
        //     //The line below "resets" the materials
        //     renderers[i].material.color = Color.blue;
        // }
        // renderers = abc_garment.GetComponentsInChildren<Renderer>();
        // for(int i = 0; i < renderers.Length; i++) {
        //     //The line below "resets" the materials
        //     renderers[i].material.color = Color.blue;
        // }
       

        // disable data record comp 
        this.dataRec.SetActive(false);        
        this.dataRec.GetComponent<CaptureData>().enabled = false;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
