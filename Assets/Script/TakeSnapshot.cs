using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeSnapshot : MonoBehaviour
{

    public int snapshotWidth = 512;    
    public int snapshotHeight = 512;

    string mode = "obi";
    string outDir = "Temp/Snapshots";
    Camera snapCam;

    int fileCounter = 0;  
    string outFilename;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("init......");
        // prep output dir
        this.fileCounter = 0;
        Directory.CreateDirectory(Application.dataPath + $"/Temp/");  
        Directory.CreateDirectory(Application.dataPath + $"/{outDir}");  
        
        // get camera ready
        this.snapCam = this.GetComponent<Camera>();            
        this.snapCam.targetTexture = new RenderTexture(this.snapshotWidth, snapshotHeight, 24);        
    }

    // This will be called after "phyics" and "game logic" 
    void LateUpdate()
    {
        if(this.fileCounter < 30)
        {
            string baseName = $"{mode}_body_garment_{this.fileCounter}";
            this.outFilename = Application.dataPath + $"/{this.outDir}/" +  baseName + ".png";  
            // InvokeRepeating ("GetSnapshot", 2f, 0.5f);

            StartCoroutine(GetSnapshot());
            this.fileCounter++;
        }
    }
    

    IEnumerator GetSnapshot()
    {
               
        try
        {
            // create a buffer => render snapshot cam => active render texture tp
            Texture2D screenShot = new Texture2D(this.snapshotWidth, this.snapshotHeight, TextureFormat.RGB24, false);
            this.snapCam.Render();
            RenderTexture.active = this.snapCam.targetTexture;

            screenShot.ReadPixels(new Rect(0, 0, this.snapshotWidth, this.snapshotHeight), 0, 0); // read data from cur active render 
            screenShot.Apply();
            byte[] bytes = screenShot.EncodeToPNG();                    
            File.WriteAllBytes(outFilename, bytes);               

            Debug.Log(string.Format("Took screenshot to: {0}", outFilename));            
        }
        catch(Exception e)
        {
            Debug.Log(e.ToString());
            Debug.Log($"fail to capture frame {fileCounter}: {outFilename}");                
        }
        yield return new WaitForEndOfFrame();         
    }

}
