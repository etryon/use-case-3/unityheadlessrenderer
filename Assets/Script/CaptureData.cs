﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Obi;
// using UnityEditor;

public class CaptureData : MonoBehaviour
{
    int fileCounter = 0;    
    public int numSamples = 30; 
    float samplingRateInSec = 0.1f;
    
    public int snapshotWidth = 512;    
    public int snapshotHeight = 512;
    
    public GameObject abcGameObj;
    public GameObject obiGameObj;     
    Camera abcCam;        
    Camera obiCam;        
    
    float [] timestamps;
    string outDir = "Temp/Snapshots";    
    public string suffix = "_bg";
    // bool captureInProg = false;    
    // public bool terminateAtFinish = true;
    
    // Start is called before the first frame update
    void Start()
    {   
        Debug.Log("init......");
        // prep output dir
        this.fileCounter = 0;
        Directory.CreateDirectory(Application.dataPath + $"/Temp");
        Directory.CreateDirectory(Application.dataPath + $"/Temp/Snapshots");        
                
        // define texture camera 
        this.abcCam = this.GetCameraReady(this.abcGameObj);
        this.obiCam = this.GetCameraReady(this.obiGameObj);        
        
        if (this.abcCam && this.obiCam)
        {
            this.samplingRateInSec = this.GetDeltaTime();
            Debug.Log($"est. delta time: {this.samplingRateInSec}");        
        }        

        // set up obi assets        
    }

    IEnumerator ExampleCoroutine()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(5);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }

    // This will be called after "physics" and "game logic" 
    void LateUpdate()
    {
        
        if(this.fileCounter < this.numSamples)
        {
            Debug.Log($"take a snapshot for frame: {this.fileCounter}");
            // StartCoroutine(ExampleCoroutine());
            // this.TakeSnapshotsFromObi_();
            // this.TakeSnapshotsFromAlembic_();
            StartCoroutine(TakeSnapshotsFromObi_());                        
            StartCoroutine(TakeSnapshotsFromAlembic_());

            this.fileCounter++;
        }
        else //if (this.fileCounter == this.numSamples)
        {   
            Debug.Log("complete the process!");

            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();        
            #endif            
        }                
    }

    float GetDeltaTime(){
        // estimate the animation delta from the abc animation
        GameObject obj = this.abcGameObj;                
        PlayableDirector playableDirector = obj.GetComponent<PlayableDirector>();
        TimelineAsset timelineAsset = (TimelineAsset) playableDirector.playableAsset;                                        

        // processing params         
        int maxNumFrames = this.numSamples;
        float totTime = (float)(timelineAsset.duration);
        float deltaTime = totTime/maxNumFrames;  

        return deltaTime;   
    }

    IEnumerator TakeSnapshotsFromObi_()
    // void TakeSnapshotsFromObi_()
    {   
        // collect required game objects 
        GameObject obj = this.obiGameObj; // GameObject.Find("ObiAnim");
        GameObject avatar = obj.transform.Find("Avatar").gameObject;
        GameObject garment = obj.transform.Find("Garment").gameObject;
        
        Animator avatarAnim = avatar.GetComponent<Animator>();       
        Animator garmentAnim = garment.GetComponent<Animator>();       
        AnimationClip[] clips = avatarAnim.runtimeAnimatorController.animationClips;
                
        // run animation 
        string mode = "obi";                
        float delta = this.samplingRateInSec;        
        float time = (this.fileCounter)*delta;
        avatarAnim.Update(delta);
        // yield return new WaitForEndOfFrame();
        garmentAnim.Update(delta);        
        Debug.Log($"frame id: {this.fileCounter}, obi time: {time}");        
        yield return new WaitForEndOfFrame();

        // save to image 
        string baseName = $"{mode}_body_garment_{this.fileCounter}";
        string outFilename = GetSnapshotName(baseName); 
        this.TakeSnapshot(this.obiCam, outFilename, this.fileCounter);                                         
        // yield return new WaitForEndOfFrame();
    }

    IEnumerator TakeSnapshotsFromAlembic_()
    // void TakeSnapshotsFromAlembic_()
    {
        // collect required game objects 
        GameObject obj = this.abcGameObj;                
        PlayableDirector playableDirector = obj.GetComponent<PlayableDirector>();
        TimelineAsset timelineAsset = (TimelineAsset) playableDirector.playableAsset;
        //Debug.Log($"{timelineAsset.duration}");                                

        // // processing params 
        string mode = "abc";                
        float time = (float)(this.fileCounter * this.samplingRateInSec);
        playableDirector.time += this.samplingRateInSec; // time; // this.fileCounter * 0.2; //
        Debug.Log($"frame id: {this.fileCounter}, abc time: {time}");        
        playableDirector.Evaluate();
        yield return new WaitForEndOfFrame(); // WaitForSeconds(.2f); // need to small wait to finish animation
            
        // export snapshots from animation            
        // 0. avatar+garment 
        //     ActivateAvatar(obj, true);
        //     ActivateGarment(obj, true);        
        string baseName = $"{mode}_body_garment_{this.fileCounter}";
        string outFilename = GetSnapshotName(baseName); 
        this.TakeSnapshot(this.abcCam, outFilename, this.fileCounter);                                             
        // yield return new WaitForEndOfFrame();
    }

    Camera GetCameraReady(GameObject gameObj)
    {        
        // get camera and assign render texture to it
        GameObject camObj = gameObj.transform.Find("Camera").gameObject;
        Camera snapCam = null;

        if (camObj)
        {
            snapCam = camObj.GetComponent<Camera>();            
            snapCam.targetTexture = new RenderTexture(this.snapshotWidth, snapshotHeight, 24);        
        }
        else
            Debug.Log("Fail to set up the camera for " + gameObj.name);            
        
        return snapCam;
    }

    void TakeSnapshot(Camera snapCam, string filename, int fileCounter=0)
    {
               
        try
        {
            // create a buffer => render snapshot cam => active render texture tp
            Texture2D screenShot = new Texture2D(this.snapshotWidth, this.snapshotHeight, TextureFormat.RGB24, false);
            snapCam.Render();
            RenderTexture.active = snapCam.targetTexture;

            screenShot.ReadPixels(new Rect(0, 0, this.snapshotWidth, this.snapshotHeight), 0, 0); // read data from cur active render 
            screenShot.Apply();
            byte[] bytes = screenShot.EncodeToPNG();                    
            File.WriteAllBytes(filename, bytes);            

            Debug.Log(string.Format("Took screenshot to: {0}", filename));            
        }
        catch(Exception e)
        {
            Debug.Log(e.ToString());
            Debug.Log($"fail to capture frame {fileCounter}: {filename}");
        }

    }

    string GetSnapshotName(string baseFilename)
    {
        string outFileName = Application.dataPath + $"/{this.outDir}/" +  baseFilename + suffix + ".png"; //+ $"_{FileCounter}.png"; 
        return outFileName;
    }

    // void ActivateGarment(GameObject obj, bool bActivate)
    // {        
    //     GameObject garment = obj.transform.Find("Garment").gameObject;
    //     garment.SetActive(bActivate);
    // }

    // void ActivateAvatar(GameObject obj, bool bActivate)
    // {
    //     GameObject avatar = obj.transform.Find("Avatar").gameObject;
    //     avatar.SetActive( bActivate );        
    // }

    // void ActivateAllGarments(bool bActivate)
    // {
    //     ActivateGarment(this.bwAnim, bActivate);
    //     ActivateAvatar(this.bwAnim, bActivate);        
    //     ActivateGarment(this.obiAnim, bActivate);        
    //     ActivateAvatar(this.obiAnim, bActivate);
    // }

    /*
    IEnumerator TakeSnapshotsFromObi()
    {
        yield return new WaitUntil(()=> !this.captureInProg);
        this.captureInProg = true;

        GameObject obj = this.obiGameObj; // GameObject.Find("ObiAnim");
        GameObject avatar = obj.transform.Find("Avatar").gameObject;
        GameObject garment = obj.transform.Find("Garment").gameObject;
        
        Animator avatarAnim = avatar.GetComponent<Animator>();       
        Animator garmentAnim = garment.GetComponent<Animator>();       
        AnimationClip[] clips = avatarAnim.runtimeAnimatorController.animationClips;
        
        // processing params 
        string mode = "obi";        
        int maxNumframes = this.numSamples;
        float delta_time = this.samplingRateInSec;        
                
        avatarAnim.speed = 0f;   // set animation descrete 
        garmentAnim.speed = 0f;             
        for (int i = 0; i < maxNumframes; i++)
        {   
            float time = i * delta_time;
            //Debug.Log($"obi time: {time}");         
            avatarAnim.Play("body_anim", -1, time);
            yield return new WaitForEndOfFrame();
            garmentAnim.Play("garm_anim", -1, time);
            yield return new WaitForEndOfFrame();                        

            // export snapshots from animation            
            // 0. avatar+garment 
            // obj.transform.position = new Vector3(0, 0, 0);     
            // ActivateAvatar(obj, true);
            // avatar_anim.Play("mixamo", -1, i*delta_time);
            // garment_anim.Play("mixamo", -1, i*delta_time);
            // yield return new WaitForEndOfFrame();

            //ActivateGarment(obj, true);                                  
            string baseName = $"{mode}_body_garment_{i}";
            string outFilename = GetSnapshotName(baseName); 
            this.TakeSnapshot(this.obiCam, outFilename, i);             
            //obj.transform.position = new Vector3(2, 0, 0); 

            // 1. garment 
           // garment.transform.position = new Vector3(0, 0, 0);         
            // ActivateAvatar(obj, false);            
            // avatar_anim.Play("mixamo", -1, i*delta_time);
            // garment_anim.Play("mixamo", -1, i*delta_time);
            // yield return new WaitForEndOfFrame();

            // modelName = $"{mode}_garment_{i}";
            // TakeSnapshot(modelName);            
            //garment.transform.position = new Vector3(2, 0, 0);            

        //     // 2. avatar 
        //     //avatar.transform.position = new Vector3(0, 0, 0); 
        //     ActivateAvatar(obj, true);
        //     ActivateGarment(obj, false);           
        //     modelName = $"{mode}_body_{i}";
        //     TakeSnapshot(modelName);            
            // avatar.transform.position = new Vector3(2, 0, 0);              
        }
       
        avatarAnim.speed = 1f;
        garmentAnim.speed = 1f;
        this.captureInProg = false;   

    }

    IEnumerator TakeSnapshotsFromAlembic()
    {        
        // stop running if the prev. capture is still in prog
        yield return new WaitUntil(()=>!this.captureInProg); 
        this.captureInProg = true;

        // collect required game objects 
        GameObject obj = this.abcGameObj;                
        PlayableDirector playableDirector = obj.GetComponent<PlayableDirector>();
        TimelineAsset timelineAsset = (TimelineAsset) playableDirector.playableAsset;
        //Debug.Log($"{timelineAsset.duration}");                                

        // processing params 
        string mode = "abc";        
        int maxNumFrames = this.numSamples;
        float totTime = (float)(timelineAsset.duration);
        float deltaTime = totTime/maxNumFrames;  //this.samplingRateInSec; //
        Debug.Log($"tot anim time:{totTime}, max frames: {maxNumFrames}, delta time: {deltaTime}");                                
        
        // player.Time = 0.1;        
        // playableDirector.Play();
        // playableDirector.Pause();       
        // Vector3 oldPos = this.snapCam.transform.position;
        
        // this.snapCam.GetComponent<Transform>().Translate(-2,0,0);
        // ActivateGarment(obj, true);
        // ActivateAvatar(obj, true);    
        for (int i = 0; i < maxNumFrames; i++)
        {            
            // playableDirector.Play();
            float time = (float)(i * deltaTime);
            playableDirector.time = time; // this.fileCounter * 0.2; //
            Debug.Log($"frame id: {i}, abc time: {time}");
        //     // playableDirector.RebuildGraph();
        //     // playableDirector.Resume();
        //     // playableDirector.Pause();            
            playableDirector.Evaluate();
            yield return new WaitForEndOfFrame(); // WaitForSeconds(.2f); // need to small wait to finish animation
            
            // export snapshots from animation            
            // 0. avatar+garment 
        //     ActivateAvatar(obj, true);
        //     ActivateGarment(obj, true);
        //     //obj.transform.position = new Vector3(0, 0, 0);            
            string baseName = $"{mode}_body_garment_{i}";
            string outFilename = GetSnapshotName(baseName); 
            this.TakeSnapshot(this.abcCam, outFilename, i);            
        //     //obj.transform.position = new Vector3(2, 0, 0);            

        //     // 1. garment 
        //     ActivateAvatar(obj, false);
        //     //garment.transform.position = new Vector3(0, 0, 0);            
        //     modelName = $"{mode}_garment_{i}";
        //     TakeSnapshot(modelName);            
        //     // //garment.transform.position = new Vector3(2, 0, 0);            

        //     // 2. avatar 
        //     // //avatar.transform.position = new Vector3(0, 0, 0);    
        //     ActivateAvatar(obj, true);
        //     ActivateGarment(obj, false);        
        //     modelName = $"{mode}_body_{i}";
        //     TakeSnapshot(modelName);            
        //     // //avatar.transform.position = new Vector3(2, 0, 0);                                               
        }        
        // //ActivateAllGarments(true);
        // ActivateGarment(obj, true);
        // ActivateAvatar(obj, true);            
        // this.snapCam.GetComponent<Transform>().Translate(2,0,0);
        this.captureInProg = false;
    }
    */
    
}
