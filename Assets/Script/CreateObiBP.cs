using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
#if UNITY_EDITOR
    using UnityEditor;
#endif
using System.IO;


public class CreateObiBP : MonoBehaviour
{
    // public StitchSkinned stitchOBI;
    public delegate void OnObiAdded();
    public static OnObiAdded obiAdded;
    Animator animator;

    // comments added by DS
    //  * obi blueprints will be saved in "Assets/Resources/{garmentDir}/Obi/ObiBlueprints" folder, so that we can access them from the headless unity build later
    //  * obi cloth parameter imgs will be read from "Assets/Resources/{garmentDir}/Obi/ObiParamImgs" folder 
    //  * and these img maps will be scaled manually or by optimised values found in the obi optimiser.
    string resourceDir = "Resources";                   // accessed by unity only
    public string garmentDir = "";                      // this should be defined by /Assets/Temp/config.json
    public bool needToCreateAssets = false;             // if ture create obi blueprints; if false, find blueprints and attach them
    
    // these will be defined manually or you can use optimised scales
    // when you played the game it will be autometically set with the values defined 
    // in /Assets/Temp/config.json
    public float priorStatScale = 0.01f;
    public float backstopRadiusScale = 0.01f;
    public float backstop = -0.01f;
    

    void Start()
    {
        // if (!Directory.Exists(Application.dataPath + this.resourceDir))
        //     Directory.CreateDirectory(Application.dataPath + this.resourceDir); 
        
        if(this.needToCreateAssets)
        {              
            Debug.Log("creating Obi assets...");            
            StartCoroutine(CreateOBI());
        }
        else
        {            
            Debug.Log("attaching Obi assets...");
            StartCoroutine(AttachOBI());                        
        }            
            
    }


    // IEnumerator AttachOBI()
    IEnumerator AttachOBI()
    {        
        GameObject CreateObi = this.gameObject;        

        ObiSolver obs = CreateObi.AddComponent<ObiSolver>();
        ObiLateFixedUpdater olfu = CreateObi.AddComponent<ObiLateFixedUpdater>();
        olfu.solvers.Add(obs);
        olfu.enabled = false;
               
        // int counter = 0;        
        foreach (Transform child in CreateObi.transform)
        {
            GameObject childobject = child.gameObject;
            SkinnedMeshRenderer skin = childobject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (skin == null) continue;            
            Debug.Log($"adding obi asset for {childobject.name}");

            // add blueprints 
            ObiSkinnedClothBlueprint blueprint = ReadObiBlueprint(childobject.name);
            if (blueprint == null)
            {
                Debug.Log($"blueprint is not ready for {childobject.name}");
                continue;
            }         
            ObiSkinnedCloth obsc = childobject.AddComponent<ObiSkinnedCloth>();        
            obsc.skinnedClothBlueprint = blueprint;    
            ObiSkinnedClothRenderer obscr = childobject.AddComponent<ObiSkinnedClothRenderer>(); 

            // read obi param imgs
            #if UNITY_EDITOR 
                string filename =  Application.dataPath + "/" + this.resourceDir +   
                                    $"/{garmentDir}/Obi/ObiParamImgs/" + childobject.name + ".png";            
                Texture2D probMap = LoadParamImg(filename);
                // Texture2D probMap = (Texture2D)AssetDatabase.LoadAssetAtPath(filename, typeof(Texture2D));
            #endif
            #if !UNITY_EDITOR 
                string filename = $"{garmentDir}/Obi/ObiParamImgs/" + childobject.name;
                Texture2D probMap_t = Resources.Load(filename) as Texture2D; // this will not give read/write texture in runtime
                Texture2D probMap = duplicateTexture(probMap_t);             // to avoid, just create duplicate it 
                // string texture = "Assets/Resources/"+ $"{garmentDir}/Obi/ObiParamImgs/" + childobject.name;            
                // Texture2D probMap = LoadParamImg(filename);
            #endif
            
            if (probMap == null) 
            {   
                Debug.Log($"cannot find obi param img for {childobject.name} at {filename}");                
            }
            else
            {                
                Debug.Log($"loading obi param from {filename}");
                Vector2[] uvs = blueprint.inputMesh.uv;                                   
                var skinConstraints = blueprint.GetConstraintsByType(Oni.ConstraintType.Skin) as ObiConstraints<ObiSkinConstraintsBatch>;
                var skinBatch = skinConstraints.batches[0];                             // particle radius constraints are batch 0
                for (int i = 0; i < skinBatch.constraintCount; ++i)
                {               
                    int particleIndex = blueprint.topology.rawToWelded[i];              // in our case, particle id == vert id
                    var color = probMap.GetPixelBilinear(uvs[i].x, uvs[i].y);           // read uv image data
                    //Debug.Log(color.r);
                    float skinRadius = color.r * this.priorStatScale;
                    // if(counter ==4)
                    //     Debug.Log($"{skinRadius}, {color.r}" );
                    // skinBatch.skinRadiiBackstop[i * 3] = skinRadius;                    // skin radidus 
                    // skinBatch.skinRadiiBackstop[i * 3 + 1] = 4*skinRadius;              // skin backstop radius 
                    // skinBatch.skinRadiiBackstop[i * 3 + 2] = this.backstop;             // skin backstop 
                    skinBatch.skinRadiiBackstop[i * 3] = skinRadius;   // this.priorStatScale; //           
                    skinBatch.skinRadiiBackstop[i * 3 + 1] = skinRadius * this.backstopRadiusScale; // this.backstopRadiusScale; //              // skin backstop radius 
                    skinBatch.skinRadiiBackstop[i * 3 + 2] = this.backstop;
                }
            }

            // counter++;
        }
        
        obiAdded?.Invoke();
        olfu.enabled = true;
        yield return new WaitForEndOfFrame(); 
    }


    Texture2D LoadParamImg(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (System.IO.File.Exists(filePath))
        {
            fileData = System.IO.File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }

        return tex;
    }

    Texture2D duplicateTexture(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                                                            source.width,
                                                            source.height,
                                                            0,
                                                            RenderTextureFormat.Default,
                                                            RenderTextureReadWrite.Linear);
                                            
        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }


    private ObiSkinnedClothBlueprint ReadObiBlueprint(string obiAssetName)
    {
        ObiSkinnedClothBlueprint blueprint;

        #if UNITY_EDITOR 
            blueprint = (ObiSkinnedClothBlueprint)AssetDatabase.LoadAssetAtPath($"Assets/Resources/{garmentDir}/Obi/ObiBlueprints/" + obiAssetName + ".asset", typeof(ObiSkinnedClothBlueprint));
        #endif 

        #if !UNITY_EDITOR             
            blueprint = Resources.Load<ObiSkinnedClothBlueprint>($"{garmentDir}/Obi/ObiBlueprints/"+obiAssetName);            
        #endif

        return blueprint;
    }


    private IEnumerator CreateOBI()
    {
        GameObject CreateObi = this.gameObject;        

        ObiSolver obs = CreateObi.AddComponent<ObiSolver>();
        //obs.enabled = false;
        ObiLateFixedUpdater olfu = CreateObi.AddComponent<ObiLateFixedUpdater>();
        olfu.solvers.Add(obs);
        olfu.enabled = false;

        int garmentID = 0;
        foreach (Transform child in CreateObi.transform)
        {
            GameObject childobject = child.gameObject;
            SkinnedMeshRenderer skin = childobject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;                        
            if (skin == null) continue;
            
            // set up Obi blueprints and link mesh
            Debug.Log($"create obi asset for {childobject.name}");                            
            var blueprint = ScriptableObject.CreateInstance<ObiSkinnedClothBlueprint>();                    
            var mesh = skin.sharedMesh;              
            blueprint.name = childobject.name;             
            blueprint.inputMesh = mesh;           
            yield return StartCoroutine(blueprint.Generate());                                            
            
            // add the result to "asset folder"    
            // nb. you cannot create and save obi blueprints from the headless unity build
            #if UNITY_EDITOR      
                AssetDatabase.CreateAsset(blueprint, 
                                        $"Assets/{resourceDir}/{garmentDir}/Obi/ObiBlueprints/" + blueprint.name + ".asset");    
                                        //Application.dataPath + "/" + this.resourceDir + "/" + blueprint.name + ".asset");            
                AssetDatabase.SaveAssets();   
            #endif

            ObiSkinnedCloth obsc = childobject.AddComponent<ObiSkinnedCloth>();        
            obsc.skinnedClothBlueprint = blueprint;    
            ObiSkinnedClothRenderer obscr = childobject.AddComponent<ObiSkinnedClothRenderer>();            
           
            // read obi param imgs            
            string filename =  Application.dataPath + "/" + this.resourceDir +   
                            "/ObiParamImgs/" + childobject.name + ".png";       
            Texture2D probMap = LoadParamImg(filename);
    
            if (probMap == null) 
            {   
                Debug.Log($"cannot find the obi parameter img for {childobject.name}");                
            }
            else
            {
                Debug.Log($"loading obi param from {filename}");
                Vector2[] uvs = blueprint.inputMesh.uv;                                   
                var skinConstraints = blueprint.GetConstraintsByType(Oni.ConstraintType.Skin) as ObiConstraints<ObiSkinConstraintsBatch>;
                var skinBatch = skinConstraints.batches[0];                             // particle radius constraints are batch 0
                for (int i = 0; i < skinBatch.constraintCount; ++i)
                {               
                    int particleIndex = blueprint.topology.rawToWelded[i];              // in our case, particle id == vert id
                    var color = probMap.GetPixelBilinear(uvs[i].x, uvs[i].y);           // read uv image data
                    float skinRadius = color.r * this.priorStatScale;

                    skinBatch.skinRadiiBackstop[i * 3] = skinRadius;                    // skin radidus 
                    skinBatch.skinRadiiBackstop[i * 3 + 1] = this.backstopRadiusScale * skinRadius;              // skin backstop radius 
                    skinBatch.skinRadiiBackstop[i * 3 + 2] = this.backstop;             // skin backstop 
                }
            }

            garmentID++;
        }
                
        obiAdded?.Invoke();         // invoking stitching         
        olfu.enabled = true;        // resume obi updater         
    }

    // void LateUpdate(){
    //     if (this.updateNow){
    //         StartCoroutine(UpdateObiParams());   
    //         this.updateNow = false;
    //     }
    // }

}
