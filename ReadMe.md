# What is it

This is a Unity project for capturing rendering snapshots of a garment. 

The main functionality of this project is to simulate the garment on a moving body model using obi cloth and save the rendering results (i.e. snapshots) for the optimisation process. This results will be compared with the reference animation created from VSticher in [the Obi optimiser](git@gitlab.com:metail/rnd/obi_optimiser.git). 

To use this package, you need the following input assets;

1. reference garment animation on an animated body model (.abc)
2. target body model with the same animation as that in the reference abc file (.fbx)
3. target garment model which is rigged and matching the starting pose of the body model in 2 (.fbx)

The above assets should be saved in appropriate folders in ./Assets/Resouces, and placed properly in a game scene. 

Therere are some sample data in this project (see ./Assets/Resources/ABC, and ./Assets/Resources//FBX) to test the project out of the box.


# How to setup the project 

Please do the following steps to set Up the Unity project 

1. Clone repo from git@gitlab.com:metail/rnd/unity_headless_renderer.git 
2. Open the project from Unity hub, and set the target Unity version to 2020.3.25f1 (LTS)
3. Add the following Unity packages via the menu item "windows > package manager" 
    * obi cloth v6.3
    * alembic v2.2

After this, you are able to play or build the project.


# How to run the renderer
This project is designed to produce a headless executable. This means we normally build the project for target platforms (see how to build Unity project in https://docs.unity3d.com/Manual/PublishingBuilds.html).

Once the executalbe is ready, you should copy the following files from the Unity project to your build dir.

e.g. unity project => build

* Assets/Resources/ObiParamImgs => Resources/ObiParamImgs
* Temp/config.json => Temp/config.json

(note: obi-cloth will not work with Apple silicon build at the moment. please use an intel build if you are testing on Mac)

If you want to play the project for debugging or testing, you can simply click the play button in Unity. In this case, you need to set the config file in ./unity_headless_renderer/Assets/Temp/config.json correctly 

The effect of the processing prameters in config.json are as follows:

    * "playMode": 
        * 0: offline rendering (snapshot from each frame will be saved in ./Assets/Temp/Snapshots) 
        * 1: simple playing
    * "renderingMode":
        * 0: render both body and garment 
        * 1: render body only
        * 2: render garment only
    * "needNewObiAssets": 
        * true: create a new obi blueprint autometically. please use this option when you change garment asset 
        * false: reuse previous obibleprints
    * "useObi":
        * false: to show the effect wihout obi
    * "Srs",  "BRs",  "BS": 
        * 3 obi parameter values 
    

